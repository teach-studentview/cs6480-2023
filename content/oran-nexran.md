# NexRAN - O-RAN

Note: This assignment was derived from an [O-RAN handson session](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oran.md) developed by David Johnson and presented at the [POWDER-RENEW Mobile and Wireless Week (MWW2023)](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023). 

## Learning objectives

- Understand the workings of a sophisticated POWDER profile
- Understand how to deploy and execute a top-to-bottom O-RAN near RT RIC environment based on the NexRAN implementation 

## Prerequisites

- POWDER account and membership in `cs6480-2023` Project
- Laptop with [Chrome](https://www.google.com/chrome/) browser (most tested browser for web-based VNC sessions) 
  - Alternatively (and generally better), you can use `ssh` with X11 forwarding
    (we won't have time to set this up during class, so come prepared if you
    prefer this option, i.e., having already added your public `ssh` key to your
    account via the POWDER portal, and installed X11 on your laptop)
- *Important:* if your POWDER account does not have `bash` set as its
default shell, it is easiest to run `bash` first in any terminal you open,
since some of the demo commands use Bourne shell syntax for setting
variables.  If you prefer the C shell, there is an alternative.  Many of
the following instructions will tell you to
`. /local/repository/demo/get-env.sh`; if you want to use the C shell, run
`source /local/repository/demo/get-env.csh` instead.

## Profile, hardware, and software overview

Profile: [https://www.powderwireless.net/p/PowderProfiles/O-RAN](https://www.powderwireless.net/p/PowderProfiles/O-RAN)

This is a sophisticated profile with a variety of options and parameters associated with Open RAN functionality in the POWDER platfrom. We will be using a relatively narrow version of the profile which uses a single server-class compute node (Dell R430) to instantiate the O-RAN Alliance O-RAN Software Community (OSC) near-realtime RAN intelligent controller (RIC), a NexRAN enhanced version of the srsRAN 4G stack (with an emulated air interface) and the NexRAN xApp to realize a top-to-bottom RAN slicing use case.

## Lab overview

Our overall goal is to instantiate a POWDER experiment in which we can execute all the components described in the "NexRAN: Closed-loop RAN slicing in POWDER - A top-to-bottom open-source open-RAN use case" paper. 

Specifically:
1. Our complete setup will run on a single Linux physical machine
1. The POWDER profile will install all the necessary software (through its "setup script")
1. K8S and the OSC near-RT RIC will installed and started up as part of the startup script
1. We will use a "NexRAN enhanced" version of srsRAN to realize the underlying 4G mobile network. (Using an emulated air interface.)
1. Once our mobile network is operational, we will onboard and deploy the NexRAN xApp and "bind" the RIC plus xApp to the underlying mobile network
1. We will then use the NexRAN northbound API (API to a "RAN Slicing Manager") to manipulate the underlying RAN

## Hands-on

<!--
### [Instantiate your experiment](https://utah.instructure.com/courses/884883/assignments/13192707)
--->
### Instantiate your experiment

1. Log into the POWDER portal at [https://www.powderwireless.net](https://www.powderwireless.net)
2. Select the [O-RAN profile](https://www.powderwireless.net/p/PowderProfiles/O-RAN) (click the link provided) and click "Next"
3. **IMPORTANT**: If you want to use VNC to access your node: On the "2. Parameterize" panel, make sure to check the option to "Install VNC on the first node" and click "Next"
3. If you see a "Project" selection box on the "Finalize" page, select the cs6480-2023 project and click "Next". Else, just click "Next". Don't enter an experiment name, the portal will create a random one for you that will avoid collision with other experiment names.
4. Click "Finish" on the "Schedule" page. Don't enter a start or end date/time.

### Start interacting with your experiment

Once the node is booted with the appropriate OS image (i.e., "Status" says "Ready" in the "List View" tab), the profile will execute a startup script. This will take a while and we will not be able to actually start the handson session until the "Startup" says "Finished" (again in the "List View" tab). However you should be able to ssh into your node, or make use of the VNC interface fairly quickly after the node "Status" indicates "Ready". Do that as soon as you can and execute the following command in an ssh window (or xterm in VNC) to see what the startup script is doing:

```
tail -f /local/logs/setup.log
```
Note: It will take approximately 25 minutes for the startup script to complete. When it is done when you see the following output:

`mail -s ORAN/Kubernetes Instance Setup Complete`


### Handson

Once the startup script completes you can start the actual handson session.

You will need at least 6 ssh windows (or xterms) for this session. (Would be useful to start them up now.)

<!---
#### [Create an end-to-end 4G network using srsRAN](https://utah.instructure.com/courses/884883/assignments/13192716)
-->

#### Create an end-to-end 4G network using srsRAN


In one of your ssh windows, start up the 4G core network:

```
sudo /local/setup/srslte-ric/build/srsepc/src/srsepc \
--spgw.sgi_if_addr=192.168.0.1
```

In another ssh window, start up the RAN node (an eNodeB in 4G terminology).

Pick up the necessary environment variables:
```
. /local/repository/demo/get-env.sh
```

start up the eNodeB:
```
sudo /local/setup/srslte-ric/build/srsenb/src/srsenb  \
--enb.n_prb=15 --enb.name=enb1 --enb.enb_id=0x19B --rf.device_name=zmq \
--rf.device_args="fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001"  \
--ric.agent.remote_ipv4_addr=${E2TERM_SCTP}  \
--ric.agent.local_ipv4_addr=10.10.1.1 --ric.agent.local_port=52525  \
--log.all_level=warn --ric.agent.log_level=debug --log.filename=stdout  \
--slicer.enable=1 --slicer.workshare=0
```

In another ssh window, create a separate namespace for the UE:
```
sudo ip netns add ue1
```

start up the UE:
```
sudo /local/setup/srslte-ric/build/srsue/src/srsue     \
--rf.device_name=zmq \
--rf.device_args="tx_port=tcp://*:2001,rx_port=tcp://localhost:2000,id=ue,base_srate=23.04e6" \
--usim.algo=xor --usim.imsi=001010123456789 \
--usim.k=00112233445566778899aabbccddeeff --usim.imei=353490069873310  \
--log.all_level=warn --log.filename=stdout --gw.netns=ue1
```

Verify the functionality of your 4G network by pinging the interface between the core network and the "data network" (the SGi interface in 4G). In another ssh window execute:
```
sudo ip netns exec ue1 ping 192.168.0.1
```

Kill the ping and start up iperf to create traffic across the "emulated RF link" in the downlink direction. Start up an iperf server attached to the "data network" interface on the core network:
```
iperf3 -s -B 192.168.0.1 -p 5010 -i 1
```

In another ssh window start up an iperf client in the UE namespace:
```
sudo ip netns exec ue1 iperf3 -c 192.168.0.1 -p 5010 -i 1 -t 36000 -R
```

Make a note of the iperf downlink throughput reported on the UE.


<!---
#### [Verify that the RIC is running and start up the xApp](https://utah.instructure.com/courses/884883/assignments/13193683)
-->

#### Verify that the RIC is running and start up the xApp



The OSC near-RT-RIC should have been started up as a result of the experiment startup script executing. Verify that thia is the case by running the following commands in an ssh window. (All deployments should be "Available", and all pods should be "Running".)
```
kubectl -n ricplt get deployments
```
```
kubectl -n ricplt get pods 
```

##### Run the NexRAN xApp:

Onboard the nexran xApp:

```
/local/setup/oran/dms_cli onboard \
    /local/profile-public/nexran-config-file.json \
    /local/setup/oran/xapp-embedded-schema.json
```

Verify that the xApp was successfully created (you should see a JSON blob that refers to a Helm chart):

```
/local/setup/oran/dms_cli get_charts_list
```

Deploy the nexran xApp:

```
/local/setup/oran/dms_cli install \
    --xapp_chart_name=nexran --version=0.1.0 --namespace=ricxapp
```

Verify that you can interact with the nexran xApp northbound API.

Run the following script to pick up the necessary environment variables:

```
. /local/repository/demo/get-env.sh
```

Invoke the "version" method in the northbound API:

```
curl -i -X GET http://${NEXRAN_XAPP}:8000/v1/version ; echo ; echo
```

Point your browser to the online NexRAN nortbound API definition:

[https://gitlab.flux.utah.edu/powderrenewpublic/nexran/-/blob/master/etc/northbound-openapi.json](https://gitlab.flux.utah.edu/powderrenewpublic/nexran/-/blob/master/etc/northbound-openapi.json)



#### Explore and interact with the NexRAN northbound API


If you are using a new ssh windwi, run the following script to pick up the necessary environment variables:

```
. /local/repository/demo/get-env.sh
``` 

Show the objects currently associated with the NexRAN setup. (There should not be anyhting...)

Show current eNodeBs:
```
curl -i -X GET http://${NEXRAN_XAPP}:8000/v1/nodebs ; echo ; echo
```

Show current slices:
```
curl -i -X GET http://${NEXRAN_XAPP}:8000/v1/slices ; echo ; echo
```

Show current UEs:
```
curl -i -X GET http://${NEXRAN_XAPP}:8000/v1/ues ; echo ; echo
```

Associate your eNodeB with the near-RT-RIC and NexRAN xApp:
```
curl -X POST -H "Content-type: application/json" \
-d '{"type":"eNB","id":411,"mcc":"001","mnc":"01"}' http://${NEXRAN_XAPP}:8000/v1/nodebs ; echo ; echo
```
Create a "fast" slice:
```
curl -i -X POST -H "Content-type: application/json" \
-d '{"name":"fast","allocation_policy":{"type":"proportional","share":1024}}' http://${NEXRAN_XAPP}:8000/v1/slices ; echo ; echo
```

Create a "slow" slice:
```
curl -i -X POST -H "Content-type: application/json" \
-d '{"name":"slow","allocation_policy":{"type":"proportional","share":128}}' http://${NEXRAN_XAPP}:8000/v1/slices ; echo ; echo
```

Associate fast slice with eNodeb:
```
curl -i -X POST http://${NEXRAN_XAPP}:8000/v1/nodebs/enB_macro_001_001_00019b/slices/fast ; echo ; echo
```

Associate slow slice with eNodeB:
```
curl -i -X POST http://${NEXRAN_XAPP}:8000/v1/nodebs/enB_macro_001_001_00019b/slices/slow ; echo ; echo
```

Create a UE:
```
curl -i -X POST -H "Content-type: application/json" \
-d '{"imsi":"001010123456789"}' http://${NEXRAN_XAPP}:8000/v1/ues ; echo ; echo
```

Bind UE to the slow slice:
```
curl -i -X POST http://${NEXRAN_XAPP}:8000/v1/slices/slow/ues/001010123456789 ; echo ; echo
```

Note the iperf download throughput...

<!---
Construct appropriate curl commands to make the following changes to your setep. **Note the change in download throughput based on each change**:

1. Increase the slow slice relative allocation from 128 to 256, 512 and 1024
1. Reset the relative allocation for the slow slice to 256 
1. Delete the fast slice
1. Create new slice ("newfast") and associate with the eNodeB 

Use your knowledge of the NexRAN scheduler implementation to explain your results.
-->


